# CTEC V2

CTEC V2 is the clean theme for the current CTEC Site. See [http://ctec.fcoe.org](http://ctec.fcoe.org).

This theme is a child theme of the [Zurb Foundation Drupal Theme](https://www.drupal.org/project/zurb_foundation).

## Development

The site uses Gulp and Lando. Set the port in BrowserSync's config within `gulpfile.js` to match the port used in Lando.

```bash
lando gulp watch
```
CSS and JS will compile automatically when a template or css file is changed.


## Deployment

There is a second reference to the `ctec.css` file in `html.html.twig` that is commented out. Uncomment this if there are any issues with CSS loading.

