//// Gulpfile Version 0.9
// For use with Zurb Foundation in Drupal 9

const {src, dest, watch, series} = require('gulp');
const sass = require('gulp-sass')(require('sass-embedded'));
const postcss = require('gulp-postcss');
const cleancss = require('postcss-clean');
const terser = require('gulp-terser');
const browser = require('browser-sync').create();

//// Load settings from config.yml
// Updated method used based on zurb template
const yaml = require('js-yaml');
const fs = require('fs-extra');

function loadConfig() {
    const unsafe = require('js-yaml-js-types').all;
    const schema = yaml.DEFAULT_SCHEMA.extend(unsafe);
    const ymlFile = fs.readFileSync('config.yml', 'utf8');
    return yaml.load(ymlFile, {schema});
  }

const { BROWSERSYNC, PATHS } = loadConfig();

//// Gulp Sass Build Task
// Build scss -> css with sass-embedded then stream
function buildTask(){
    return src(PATHS.scss, {sourcemaps: true })
        .pipe(sass({
            includePaths: PATHS.foundationScss
        }).on('error', sass.logError))
        .pipe(postcss([cleancss()]))
        .pipe(dest('css'))
        .pipe(browser.stream());
}

//// Javascript Build Task
// Using terser to compile
function jsTask(){
    return src(PATHS.js, {sourcemaps: true })
        .pipe(terser())
        .pipe(dest('dist', {sourcemaps: '.' }));
}

//// Browsersync Initialize Server Task
// Initializes Browsersync server with project URL
function browserServe(cb) {
    browser.init({
        host: BROWSERSYNC,
        proxy: {
            target: BROWSERSYNC
        },
        open: false,
        ui: false
    }, cb());
}

//// Browsersync Reload Webpage Function
// Completely refreshes the page across browsers
function browserReload(cb){
    browser.reload();
    cb();
}

//// Gulp Watch Setup
function watchTask(){
    watch(PATHS.twig, browserReload);
    // If @imports or $variables are changed then a page reload is needed
    watch(PATHS.scss, series(buildTask, browserReload));
    // If scss is changed then compile and stream
    watch(PATHS.componentScss, buildTask);
    // If js is changed then a page reload is needed
    watch(PATHS.js, series(jsTask, browserReload));
}

//// Run Gulp Tasks
// ddev yarn run gulp
exports.default = series(
    jsTask,
    buildTask,
    browserServe,
    watchTask
)
// ddev yarn run watch
exports.watch = series(
    browserServe,
    watchTask
)