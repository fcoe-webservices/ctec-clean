/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.exampleBehavior = {
      attach: function (context, settings) {
        $('.slideshow.slideshow--home').slick({
          dots: true,
          infinite: true,
          speed: 500,
          fade: true,
          cssEase: 'linear',
          autoplay: true,
          autoplaySpeed: 9000,
        });
  
  
      },
      attach: function (context, settings) {
        $('.field-block-content--field-story-reference > .field-items').attr('data-equalizer', 'image');
        $('.stories-img').attr('data-equalizer-watch', 'image');
      
        //ig equalizer
        $('.ig-block').attr('data-equalizer', 'ig-img');
        $('.ig-block > .ig-post > .views-field > .field-content > .ig-container > .ig-inner-container').attr('data-equalizer-watch', 'ig-img');
        
        // add autoplay attributes to hero block
        const heroVideo = $('.hero-video-field-item > div > .field-media--field-media-video-file > .field-items > .field-item > video');
        const attributes = ["muted", "autoplay", "loop", "playsinline"];
        heroVideo.addClass('hero-video-muted');
        heroVideo.removeAttr('controls');
        heroVideo.removeAttr('height');
        heroVideo.removeAttr('width');
        
        
        heroVideo.attr({
            'autoplay': true,    // Adds the autoplay attribute
        'playsinline': true, // Adds the playsinline attribute
        'loop': true         // Adds the loop attribute
        });


        $(context).foundation();
        // who-we-are page mission & vision equalizer
        $('.who-we-are-container > .view-mode-full > .row > .medium-9 > div > div > .field-type-text-with-summary > .field-items > .field-item > .row').attr('data-equalizer', 'who-we-are');
        $('.quote > div > p').attr('data-equalizer-watch', 'who-we-are');
      }
    };
  
  })(jQuery, Drupal);