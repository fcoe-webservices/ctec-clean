/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.exampleBehavior = {
        attach: function (context, settings) {
            // add data-equalizer for the description section
            const cardRow = $('.card-item').parent().parent();
            //console.log($('.card-item').parent());
            //if ($('.card-item').parent())
            // for (let item of $('.card-item').parent()) {
            //     console.log(item);
            //     if (item.hasClass('column-4')) {
            //         console.log(item);
            //     }
            // }
            // if ($('.card-item').hasClass('medium-4')) {
            //     console.log('test');
            //     cardRow.attr('data-equalizer', 'cards');
            // } else {
            //     return
            // }
            //cardRow.attr('data-equalizer', 'cards');
            
            // avoid duplication of the card-secondary-container creation
            if ($('.card-secondary-container').length <= 1) {
                cardRow.prepend('<div class="card-secondary-container"></div>');
            }

            // Appends card containers to secondary card container without duplication
            $('.card-item').parent().each(function(){
                $(this).appendTo($(this).prevAll('.card-secondary-container'));
            })


            // adds data-equalizer attribute to the secondar card container
            const cardSecondaryContainer = $('.card-secondary-container');
            cardSecondaryContainer.attr('data-equalizer', 'card-title')
            
            
            // equalizes card titles to each other
            const cardTitle = $('.card-item__title');
            cardTitle.attr('data-equalizer-watch', 'card-title');
            
            // re-initialize foundation to recognize newly attached attribute
            $(context).foundation();
        }
    };
  
  })(jQuery, Drupal);